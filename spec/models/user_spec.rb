require 'spec_helper'
require 'bcrypt'
describe User do
	 it "has a valid factory" do
	 	FactoryGirl.create(:user).should be_valid
	 end
	 it "is invalid without an e-mail" do
	 	FactoryGirl.build(:user_without_email).should_not be_valid
	 end
	 it "is invalid without a correct e-mail" do
	 	FactoryGirl.build(:user_without_correct_email).should_not be_valid
	 end
	 it "is invalid without a password" do
	 	FactoryGirl.build(:user_without_password).should_not be_valid
	 end 
	 it "is invalid without a matching password confrimation" do
	 	FactoryGirl.build(:user_without_matching_confirmation_password).should_not be_valid
	 end 
	 it "should not allow duplicate emails" do
	 	user1 = FactoryGirl.create(:user, :email => "foo@bar.com")
		user2 = FactoryGirl.build(:user, :email => "foo@bar.com").should_not be_valid
	 end
	 it "should return user with valid login data" do
	 	user1 = FactoryGirl.create(:user, :email => "foo@bar.com")
	 	User.authenticate(:email => "foo@bar.com", :password => "ruby").should eq(user1)
     end
     it "should encrypt passwords correctly" do
	 	user1 = FactoryGirl.create(:user)
	 	pwd_salt = user1.password_salt
	 	puts pwd_salt
	 	pwd_hash = BCrypt::Engine.hash_secret("ruby", pwd_salt)
	 	# Genereerib uue salti
	 	user1.encrypt_password(:salt=>pwd_salt).should eq(pwd_hash)
     end
end