FactoryGirl.define do
	factory :user do
		sequence(:email) { |n| "user#{n}@example.com" }
		password "ruby"
		password_confirmation "ruby"
	end

	factory :user_without_email, :parent=>:user do |f|
		f.email nil
	end

	factory :user_without_correct_email, :parent=>:user do |f|
		f.email "AADD"
	end

	factory :user_without_password, :parent=>:user do |f|
		f.password nil
	end

	factory :user_without_matching_confirmation_password, :parent=>:user do |f|
		f.password_confirmation "Dec"
	end
end