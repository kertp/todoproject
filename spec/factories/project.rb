require 'faker'

FactoryGirl.define do
	factory :project do |f|
		f.name "Test"
		f.description "Test"
	end
end