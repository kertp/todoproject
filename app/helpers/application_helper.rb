module ApplicationHelper

  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  def is_admin?(id=nil)
      User.find_by_id(current_user).ownerships.find_by_project_id(id || params[:id]).owner
  end

  def check_admin_status
    unless is_admin?
      redirect_to :back, :alert => "You are not admin of the project!"
    end
  end
end
