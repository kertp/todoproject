class SessionsController < ApplicationController
  before_filter :already_logged_in?, :only => [:new]
  skip_before_filter :check_user
  def new
  end

def create
  user = User.authenticate :email => params[:email], :password => params[:password]
  if user
    session[:user_id] = user.id
    redirect_to root_url
  else
    render "new", :alert =>"Invalid email or password"
  end
end

  def destroy
    session[:user_id] = nil
    redirect_to root_url
  end

  private

  def already_logged_in?
    if current_user
      redirect_to root_url, :alert => "You cant log in twice!"
    end
  end
end
