class UsersController < ApplicationController
  skip_before_filter :check_user
  before_filter :same_user?
  def new
    @user = User.new
  end

  def create
    @user = User.new(params[:user])
    if @user.save
      redirect_to root_url, :notice => "Signed up!"
    else
      render "new"
    end
  end

  def edit
    @user = User.find(params[:id])
  end

    # PUT /projects/1
  # PUT /projects/1.json
  def update
    @user = User.find(params[:id])
    respond_to do |format|
      if @user.update_attributes(params[:user])
        flash[:notice] = "Your settings were successfully updated."
        format.html { redirect_to edit_user_path(@user)}
        format.json { head :no_content }
      else
        flash[:notice] = "Something went wrong."
        format.html { render action: "edit" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def same_user?
    unless User.find_by_id(params[:id]) == current_user
      redirect_to root_url, :alert => "Please ĺeave!"
    end
  end
end
