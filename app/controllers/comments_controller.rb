class CommentsController < ApplicationController
	before_filter :check_admin_status, :only => [:destroy]

	def create
		@commentable = find_commentable
		@user = 1
		params[:comment][:user_id] = current_user
		@comment = @commentable.comments.build(params[:comment])
		if @comment.save
			flash[:notice] = "Successfully saved comment."
		else
			flash[:alert] = "A comment without content."
		end
		redirect_to :back
	end

	def destroy
		@project = Project.find(params[:project_id])
		unless params[:task_id]
			@comment = @project.comments.find(params[:id])
		else
			@task = @project.tasks.find(params[:task_id])
			@comment = @task.comments.find(params[:id])
		end
		if @comment.destroy
			flash[:notice] = "Successfully destroyed comment."
		else
			flash[:alert] = "Something went wrong."
		end
		redirect_to :back
	end

	def find_commentable
		p = Project.find(params[:project_id])
		if params[:task_id]
			t = p.tasks.find(params[:task_id])
		else
			p
		end
		t || p
	end

	def current_user
		@current_user ||= User.find(session[:user_id]) if session[:user_id]
	end

	  # Kirjutan üle, sest projecti id task kontrollerit kasutades muutuja project_id all. Muidu lihtsalt id
	  def has_access?(user_id = nil)
	    User.find_by_id(user_id || current_user).ownerships.find_by_project_id(params[:project_id])
	  end
end
