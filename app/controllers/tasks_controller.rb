class TasksController < ApplicationController
  before_filter :check_access_rights
  before_filter :check_admin_status, :only => [:edit, :update, :destroy]
  # # GET /tasks
  # # GET /tasks.json
  def index
    @project = Project.find(params[:project_id])
    @tasks = @project.tasks

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @tasks }
    end
  end

  # # GET /tasks/1
  # # GET /tasks/1.json
  def show
    @project = Project.find(params[:project_id])
    @task = @project.tasks.find(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @task }
    end
  end

  # # GET /tasks/new
  # GET /tasks/new.json
  def new
    @project = Project.find(params[:project_id])
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @project }
    end
  end

  # GET /tasks/1/edit
  def edit
    @project = Project.find(params[:project_id])
    @task = @project.tasks.find params[:id]
  end

  # POST /tasks
  # POST /tasks.json
  def create
    # Kontrollin enne, kas kasutajal on üldse ligipääsuõigused projektile
    if has_access?(params[:assignee])
      @project = Project.find_by_id(params[:project_id])
      parameters = Hash.new
      parameters[:name] = params[:name]
      parameters[:content] = params[:desc]
      parameters[:deadline] = params[:deadline]
      parameters[:closed] = params[:closed]
      parameters[:user_id] = params[:assignee]
      parameters[:project_id] = params[:project_id]
      @task = @project.tasks.build(parameters)
      if @task.save!
        redirect_to [@project, @task], :notice => "Task successfully created!"
      else
        redirect_to :back, :alert => "Something went wrong! Please check your input data"
      end
    else
      redirect_to :back, :alert => "The user doesn't have access to the project!"
    end
  end

  # PUT /tasks/1
  # PUT /tasks/1.json
  def update
    @project = Project.find(params[:project_id])
    @task = @project.tasks.find params[:id]
    respond_to do |format|
      if @task.update_attributes(params[:task])
        format.html { redirect_to project_task_path(params[:project_id], :task_id => params[:id]), notice: 'Task was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tasks/1
  # DELETE /tasks/1.json
  def destroy
    @project = Project.find(params[:project_id])
    @task = @project.tasks.find params[:id]
    @task.destroy
    respond_to do |format|
      format.html { redirect_to project_path(params[:project_id]), notice: 'Task was successfully deleted.'}
      format.json { head :no_content }
    end
  end

  private

  # Kirjutan üle, sest projecti id task kontrollerit kasutades muutuja project_id all.
  def has_access?(user_id = nil)
    User.find_by_id(user_id || current_user).ownerships.find_by_project_id(params[:project_id])
  end
end
