class ApplicationController < ActionController::Base
  include ApplicationHelper
  protect_from_forgery
  before_filter :check_user

  private

  def check_user
    unless logged_in?
      redirect_to log_in_path
    end
  end

  def logged_in?
    current_user
  end

  def check_access_rights
    unless has_access?
      redirect_to root_url, :alert => "You don't have access to the project!"
    end
  end

  def has_access?
    User.find_by_id(current_user).ownerships.find_by_project_id(params[:id])
  end

  def is_admin?
    has_access?.owner
  end
end
