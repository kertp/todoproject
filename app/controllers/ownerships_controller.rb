class OwnershipsController < ApplicationController

	def new

	end

	def create
		user = User.find_by_email(params[:e_mail])
		if user
			@project = Project.find_by_id(params[:project_id])
			parameters = Hash.new
			parameters[:user_id] = user.id
			parameters[:owner] = params[:owner]
			parameters[:project_id] = params[:project_id]
			@ownership = @project.ownerships.build(parameters)
			if @ownership.save
				redirect_to edit_project_path(@project)
			else
				render :action => 'new'
			end
		else
			flash[:alert] = "The user does not exist."
		redirect_to :back
		end
	end

	def destroy
		@ownership = Project.find(params[:project_id]).ownerships.find_by_id(params[:id])
		if @ownership.user == current_user
			flash[:alert] = "You cant do that!"
		else
			flash[:notice] = "Done!"
			@ownership.destroy
		end
		redirect_to edit_project_path(params[:project_id])
	end

	def delete

	end

end
