# == Schema Information
#
# Table name: ownerships
#
#  id         :integer          not null, primary key
#  owner      :boolean
#  project_id :integer
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Ownership < ActiveRecord::Base
  attr_accessible :project_id, :user_id, :owner
  belongs_to :project, :dependent => :delete
  belongs_to :user, :dependent => :delete
end
