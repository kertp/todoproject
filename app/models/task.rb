class Task < ActiveRecord::Base
  attr_accessible :closed, :content, :deadline, :name, :user_id, :project_id
  has_many :comments, :as => :commentable, :dependent => :destroy
  belongs_to :user
  belongs_to :project

  validates :content, :presence => true, :length => { :maximum => 200 }
  validates :deadline, :presence => true
  validates :name, :presence => true, :length => { :maximum => 50 }
  validates :user_id, :presence => true
  validates	:project_id, :presence => true
end
