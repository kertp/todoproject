Todo::Application.routes.draw do
  root :to => "projects#index"
  
  # map.resources :tasks, :has_many => :comments  
  # map.resources :projects, :has_many => :comments  
  resources :projects do
    resources :tasks do
      resources :comments
    end
    resources :comments
    resources :ownerships
  end


  
  # Kasutajad ja sessioonid
  get "log_in" => "sessions#new", :as => "log_in"
  get "log_out" => "sessions#destroy", :as => "log_out"

  get "sign_up" => "users#new"

  resources :users
  resources :sessions
end
